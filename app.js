'use strict';

// Declare app level module which depends on views, and components
var coffeeApp = angular.module('coffeeApp', [
  'ngRoute',
  'coffeeAppControllers',
  'ui.bootstrap'
  //'myApp.view1',
  //'myApp.view2',
  //'myApp.version'
]);
coffeeApp.config(['$routeProvider', function($routeProvider) {
   $routeProvider.
   when('/coffees',{
      templateUrl:'coffees/coffees.html',
      controller: 'CoffeeListCtrl'
   }).
   when('/coffees/:id',{
      templateUrl: 'coffees/reviews.html',
      controller: 'CoffeeReviewsCtrl'
   }).
   otherwise({
      redirectTo: '/coffees'
   });
}]);
